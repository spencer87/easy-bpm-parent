package com.pig.easy.bpm;

import com.alibaba.nacos.spring.context.annotation.config.EnableNacosConfig;
import com.alibaba.nacos.spring.context.annotation.config.NacosPropertySource;
import com.alibaba.nacos.spring.context.annotation.discovery.EnableNacosDiscovery;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

import java.util.concurrent.CountDownLatch;

@EnableNacosConfig
@EnableNacosDiscovery
@SpringBootApplication(exclude = DataSourceAutoConfiguration.class,scanBasePackages = {"com.pig"})
@NacosPropertySource(dataId = "easy-bpm-web.properties",groupId = "DEFAULT_GROUP", autoRefreshed = true)
public class EasyBpmWebApplication {

    public static void main(String[] args) throws InterruptedException {
        SpringApplication.run(EasyBpmWebApplication.class, args);
        CountDownLatch count = new CountDownLatch(1);
        count.await();
    }

}
