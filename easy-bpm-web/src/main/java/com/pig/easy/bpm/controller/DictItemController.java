package com.pig.easy.bpm.controller;


import com.alibaba.dubbo.config.annotation.Reference;
import com.github.pagehelper.PageInfo;
import com.pig.easy.bpm.dto.request.DictItemQueryDTO;
import com.pig.easy.bpm.dto.request.DictItemSaveOrUpdateDTO;
import com.pig.easy.bpm.dto.response.DictItemDTO;
import com.pig.easy.bpm.entityError.EntityError;
import com.pig.easy.bpm.service.DictItemService;
import com.pig.easy.bpm.utils.CommonUtils;
import com.pig.easy.bpm.utils.JsonResult;
import com.pig.easy.bpm.utils.Result;
import com.pig.easy.bpm.vo.request.DictItemQueryVO;
import com.pig.easy.bpm.vo.request.DictItemSaveOrUpdateVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

/**
 * <p>
 * 字典详细表 前端控制器
 * </p>
 *
 * @author pig
 * @since 2020-05-28
 */
@RestController
@RequestMapping("/dictItem")
@Api(tags = "字典值列表管理", value = "字典值列表管理")
public class DictItemController extends BaseController {

    @Reference
    DictItemService dictItemService;

    @ApiOperation(value = "查询字典值列表", notes = "查询字典值列表", produces = "application/json")
    @PostMapping("/getList")
    public JsonResult getList(@ApiParam(name = "字典信息", value = "传入json格式", required = true) @Valid @RequestBody DictItemQueryVO dictItemQueryVO) {

        if(dictItemQueryVO == null
                || StringUtils.isEmpty(dictItemQueryVO.getTenantId())){
            return JsonResult.error(EntityError.ILLEGAL_ARGUMENT_ERROR);
        }
        DictItemQueryDTO dictItemQueryDTO = switchToDTO(dictItemQueryVO, DictItemQueryDTO.class);
        Result<PageInfo<DictItemDTO>> result = dictItemService.getListByCondition(dictItemQueryDTO);
        if (result.getEntityError().getCode() != EntityError.SUCCESS.getCode()) {
            return JsonResult.error(result.getEntityError());
        }
        return JsonResult.success(result.getData());
    }

    @ApiOperation(value = "新增或修改字典值", notes = "新增或修改字典值", produces = "application/json")
    @PostMapping("/insertOrUpdateDictItem")
    public JsonResult insertOrUpdateDict(@ApiParam(name = "字典值信息", value = "传入json格式", required = true) @Valid @RequestBody DictItemSaveOrUpdateVO dictItemSaveOrUpdateVO) {

        if(dictItemSaveOrUpdateVO == null
                || StringUtils.isEmpty(dictItemSaveOrUpdateVO.getTenantId())
                || StringUtils.isEmpty(dictItemSaveOrUpdateVO.getItemText())
                || StringUtils.isEmpty(dictItemSaveOrUpdateVO.getItemValue())
                ){
            return JsonResult.error(EntityError.ILLEGAL_ARGUMENT_ERROR);
        }
        DictItemSaveOrUpdateDTO dictQueryDTO = switchToDTO(dictItemSaveOrUpdateVO, DictItemSaveOrUpdateDTO.class);
        dictQueryDTO.setOperatorId(currentUserInfo().getUserId());
        dictQueryDTO.setOperatorName(currentUserInfo().getRealName());
        Result<Integer> result = dictItemService.insertOrUpdate(dictQueryDTO);
        if (result.getEntityError().getCode() != EntityError.SUCCESS.getCode()) {
            return JsonResult.error(result.getEntityError());
        }
        return JsonResult.success(result.getData());
    }

    @ApiOperation(value = "删除字典值", notes = "删除字典值", produces = "application/json")
    @PostMapping("/delete")
    public JsonResult delete(@ApiParam(name = "字典值信息", value = "传入json格式", required = true) @Valid @RequestBody DictItemSaveOrUpdateVO dictItemSaveOrUpdateVO) {

        if(dictItemSaveOrUpdateVO == null
                || StringUtils.isEmpty(dictItemSaveOrUpdateVO.getTenantId())
                || CommonUtils.evalLong(dictItemSaveOrUpdateVO.getDictId()) < 0
                || CommonUtils.evalLong(dictItemSaveOrUpdateVO.getItemId()) < 0
                ){
            return JsonResult.error(EntityError.ILLEGAL_ARGUMENT_ERROR);
        }
        DictItemSaveOrUpdateDTO dictQueryDTO = switchToDTO(dictItemSaveOrUpdateVO, DictItemSaveOrUpdateDTO.class);
        dictQueryDTO.setValidState(0);
        dictQueryDTO.setOperatorId(currentUserInfo().getUserId());
        dictQueryDTO.setOperatorName(currentUserInfo().getRealName());
        Result<Integer> result = dictItemService.insertOrUpdate(dictQueryDTO);
        if (result.getEntityError().getCode() != EntityError.SUCCESS.getCode()) {
            return JsonResult.error(result.getEntityError());
        }
        return JsonResult.success(result.getData());
    }



}

