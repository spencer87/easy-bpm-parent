package com.pig.easy.bpm.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.pig.easy.bpm.dto.response.ProcessDiagramDTO;
import com.pig.easy.bpm.entityError.EntityError;
import com.pig.easy.bpm.service.ProcessDiagramService;
import com.pig.easy.bpm.utils.JsonResult;
import com.pig.easy.bpm.utils.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * todo:
 *
 * @author : pig
 * @date : 2020/7/5 18:29
 */
@Api(tags = "流程图详细信息", value = "流程图详细信息")
@RestController
@RequestMapping("/processDiagram")
public class ProcessDiagramController extends BaseController {

    @Reference
    ProcessDiagramService processDiagramService;

    @ApiOperation(value = "获取图流程详细信息", notes = "获取图流程详细信息", produces = "application/json")
    @PostMapping("/getProcessDiagramByApplyId/{applyId}")
    public JsonResult getProcessDiagramByApplyId(
            @ApiParam(required = true, name = "申请编号", value = "applyId", example = "1") @PathVariable("applyId") Long applyId
    ) {
        Result<ProcessDiagramDTO> result = processDiagramService.getProcessDiagramByApplyId(applyId);
        if (result.getEntityError().getCode() != EntityError.SUCCESS.getCode()) {
            return JsonResult.error(result.getEntityError());
        }
        return JsonResult.success(result.getData());
    }
}
