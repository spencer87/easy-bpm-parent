package com.pig.easy.bpm.event;

import com.pig.easy.bpm.entityError.EntityError;
import com.pig.easy.bpm.execption.BpmException;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * todo:
 *
 * @author : pig
 * @date : 2020/5/20 15:07
 */
public enum BestBpmEventType {

    ENTITY_CREATED,

    ENTITY_UPDATED;

    public static final BestBpmEventType[] EMPTY_ARRAY = new BestBpmEventType[]{};

    public static BestBpmEventType[] getTypesFromString(String string) {
        List<BestBpmEventType> result = new ArrayList<>();
        if (string != null && !string.isEmpty()) {
            String[] split = StringUtils.split(string, ",");
            for (String typeName : split) {
                boolean found = false;
                for (BestBpmEventType type : BestBpmEventType.values()) {
                    if (typeName.equals(type.name())) {
                        result.add(type);
                        found = true;
                        break;
                    }
                }
                if (!found) {
                    throw new BpmException(new EntityError(EntityError.ILLEGAL_ARGUMENT_ERROR.getCode(), "Invalid event-type: " + typeName));
                }
            }
        }

        return result.toArray(EMPTY_ARRAY);
    }

}
