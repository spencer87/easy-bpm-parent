package com.pig.easy.bpm.service;

import com.pig.easy.bpm.dto.request.*;
import com.pig.easy.bpm.dto.response.*;
import com.github.pagehelper.PageInfo;
import com.pig.easy.bpm.utils.Result;
/**
 * <p>
 * 租户表 服务类
 * </p>
 *
 * @author pig
 * @since 2020-09-02
 */
public interface TenantService {

        Result<PageInfo<TenantDTO>> getListByCondition(TenantQueryDTO param);

        Result<Integer> insertTenant(TenantSaveOrUpdateDTO param);

        Result<Integer> updateTenant(TenantSaveOrUpdateDTO param);

        Result<Integer> deleteTenant(TenantSaveOrUpdateDTO param);

 }
