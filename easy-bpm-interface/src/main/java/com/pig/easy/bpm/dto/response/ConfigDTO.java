package com.pig.easy.bpm.dto.response;

import lombok.Data;
import lombok.ToString;

/**
 * todo:
 *
 * @author : pig
 * @date : 2020/5/21 13:49
 */
@Data
@ToString
public class ConfigDTO extends BaseResponseDTO {

    private static final long serialVersionUID = 7559869266249639431L;

    private Long configId;

    private String configCode;

    private String configName;

    private Long templateId;

    private String configKey;

    private String configValue;

    private String configType;

    private String tenantId;

    private String remarks;

    private Long operatorId;

    private String operatorName;

}
