package com.pig.easy.bpm.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.pig.easy.bpm.dto.request.DictItemQueryDTO;
import com.pig.easy.bpm.dto.request.DictItemSaveOrUpdateDTO;
import com.pig.easy.bpm.dto.response.DictItemDTO;
import com.pig.easy.bpm.entity.DictItemDO;
import com.pig.easy.bpm.entityError.EntityError;
import com.pig.easy.bpm.mapper.DictItemMapper;
import com.pig.easy.bpm.service.DictItemService;
import com.pig.easy.bpm.utils.BeanUtils;
import com.pig.easy.bpm.utils.BestBpmAsset;
import com.pig.easy.bpm.utils.CommonUtils;
import com.pig.easy.bpm.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 字典详细表 服务实现类
 * </p>
 *
 * @author pig
 * @since 2020-05-28
 */
@com.alibaba.dubbo.config.annotation.Service
public class DictItemServiceImpl extends BeseServiceImpl<DictItemMapper, DictItemDO> implements DictItemService {

    @Autowired
    DictItemMapper dictItemMapper;

    @Override
    public Result<PageInfo<DictItemDTO>> getListByCondition(DictItemQueryDTO dictItemQueryDTO) {

        BestBpmAsset.isAssetEmpty(dictItemQueryDTO);
        BestBpmAsset.isAssetEmpty(dictItemQueryDTO.getTenantId());

        int pageIndex = CommonUtils.evalInt(dictItemQueryDTO.getPageIndex(), DEFAULT_PAGE_INDEX);
        int pageSize = CommonUtils.evalInt(dictItemQueryDTO.getPageSize(), DEFAULT_PAGE_SIZE);

        dictItemQueryDTO.setValidState(VALID_STATE);

        PageHelper.startPage(pageIndex, pageSize);
        List<DictItemDTO> dictItemDTOS = dictItemMapper.getListByCondition(dictItemQueryDTO);

        if(dictItemDTOS == null){
            dictItemDTOS = new ArrayList<>();
        }
        PageInfo<DictItemDTO> pageInfo = new PageInfo<>(dictItemDTOS);
        return Result.responseSuccess(pageInfo);
    }

    @Override
    public Result<List<DictItemDTO>> getList(DictItemQueryDTO dictItemQueryDTO) {

        int pageIndex = CommonUtils.evalInt(dictItemQueryDTO.getPageIndex(), DEFAULT_PAGE_INDEX);
        int pageSize = CommonUtils.evalInt(dictItemQueryDTO.getPageSize(), MAX_PAGE_SIZE);
        dictItemQueryDTO.setPageIndex(pageIndex);
        dictItemQueryDTO.setPageSize(pageSize);
        Result<PageInfo<DictItemDTO>> result = getListByCondition(dictItemQueryDTO);
        if(result.getEntityError().getCode() != EntityError.SUCCESS.getCode()){
            return Result.responseError(result.getEntityError());
        }
        List<DictItemDTO> resultList =result.getData().getList();
        if(resultList == null){
            resultList = new ArrayList<>();
        }
        return Result.responseSuccess(resultList);
    }

    @Override
    public Result<List<DictItemDTO>> getListByDictCode(String dictCode) {

        BestBpmAsset.isAssetEmpty(dictCode);
        List<DictItemDTO> result = dictItemMapper.getListByDictCode(dictCode);
        if(result == null){
            result = new ArrayList<>();
        }
        return Result.responseSuccess(result);
    }

    @Override
    public Result<Integer> insertOrUpdate(DictItemSaveOrUpdateDTO dictItemSaveOrUpdateDTO) {

        BestBpmAsset.isAssetEmpty(dictItemSaveOrUpdateDTO);
        BestBpmAsset.isAssetEmpty(dictItemSaveOrUpdateDTO.getTenantId());
        BestBpmAsset.isAssetEmpty(dictItemSaveOrUpdateDTO.getDictId());


        DictItemDO dictItemDO = BeanUtils.switchToDO(dictItemSaveOrUpdateDTO,DictItemDO.class);
        dictItemDO.setUpdateTime(LocalDateTime.now());
        int num = 0;
        if(CommonUtils.evalLong(dictItemSaveOrUpdateDTO.getItemId()) > 0){
            num = dictItemMapper.updateById(dictItemDO);
        } else {
            num = dictItemMapper.insert(dictItemDO);
        }
        return Result.responseSuccess(num);
    }
}
