package com.pig.easy.bpm.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.pig.easy.bpm.service.TestService;
import lombok.extern.slf4j.Slf4j;

/**
 * todo:
 *
 * @author : pig
 * @date : 2020/5/8 10:16
 */
@Service
@Slf4j
public class TestServiceImpl implements TestService {

    //@NacosValue(value = "${dubbo.server.name:}", autoRefreshed = true)
  //  private String serverName;

    @Override
    public String getTest() {

        //System.out.println("getTest serverName = " + serverName);
        return "serverName";
    }
}
