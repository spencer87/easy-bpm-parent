package com.pig.easy.bpm.service.impl;

import com.pig.easy.bpm.entity.TenantDO;
import com.pig.easy.bpm.mapper.TenantMapper;
import com.pig.easy.bpm.service.TenantService;
import com.pig.easy.bpm.service.impl.BeseServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.alibaba.dubbo.config.annotation.Service;

import com.pig.easy.bpm.utils.BeanUtils;
import com.pig.easy.bpm.utils.CommonUtils;
import com.pig.easy.bpm.utils.Result;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import com.pig.easy.bpm.entityError.EntityError;
import com.pig.easy.bpm.dto.request.*;
import com.pig.easy.bpm.dto.response.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
/**
 * <p>
 * 租户表 服务实现类
 * </p>
 *
 * @author pig
 * @since 2020-09-02
 */
@Service
public class TenantServiceImpl extends BeseServiceImpl<TenantMapper, TenantDO>implements TenantService {

        @Autowired
        TenantMapper mapper;

        @Override
        public Result<PageInfo<TenantDTO>>getListByCondition(TenantQueryDTO param){

          if(param==null){
            return Result.responseError(EntityError.ILLEGAL_ARGUMENT_ERROR);
          }
          int pageIndex=CommonUtils.evalInt(param.getPageIndex(),DEFAULT_PAGE_INDEX);
          int pageSize=CommonUtils.evalInt(param.getPageSize(),DEFAULT_PAGE_SIZE);

          PageHelper.startPage(pageIndex,pageSize);
          param.setValidState(VALID_STATE);
          List<TenantDTO>list=mapper.getListByCondition(param);
          if(list==null){
           list=new ArrayList<>();
          }
          PageInfo<TenantDTO>pageInfo=new PageInfo<>(list);
          return Result.responseSuccess(pageInfo);
        }


        @Override
        public Result<Integer>insertTenant(TenantSaveOrUpdateDTO param){

          if(param==null){
            return Result.responseError(EntityError.ILLEGAL_ARGUMENT_ERROR);
          }

          TenantDO temp=BeanUtils.switchToDO(param, TenantDO.class);
            Integer num=mapper.insert(temp);
            return Result.responseSuccess(num);
        }

         @Override
         public Result<Integer>updateTenant(TenantSaveOrUpdateDTO param){

          if(param==null){
           return Result.responseError(EntityError.ILLEGAL_ARGUMENT_ERROR);
          }

          TenantDO temp=BeanUtils.switchToDO(param, TenantDO.class);
          Integer num=mapper.updateById(temp);
          return Result.responseSuccess(num);
        }

        @Override
        public Result<Integer>deleteTenant(TenantSaveOrUpdateDTO param){

          if(param==null){
             return Result.responseError(EntityError.ILLEGAL_ARGUMENT_ERROR);
          }

          TenantDO temp=BeanUtils.switchToDO(param, TenantDO.class);
          temp.setValidState(INVALID_STATE);
          Integer num=mapper.updateById(temp);
          return Result.responseSuccess(num);
        }

}
