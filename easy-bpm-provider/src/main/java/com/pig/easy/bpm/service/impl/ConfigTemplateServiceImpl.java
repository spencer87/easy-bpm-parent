package com.pig.easy.bpm.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.pig.easy.bpm.dto.response.ConfigTemplateDTO;
import com.pig.easy.bpm.entity.ConfigTemplateDO;
import com.pig.easy.bpm.mapper.ConfigTemplateMapper;
import com.pig.easy.bpm.service.ConfigTemplateService;
import com.pig.easy.bpm.utils.BeanUtils;
import com.pig.easy.bpm.utils.BestBpmAsset;
import com.pig.easy.bpm.utils.Result;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * <p>
 * 服务实现类
 * </p>
 ConfigTemplateDO * @author pig
 * @since 2020-05-21
 */
@com.alibaba.dubbo.config.annotation.Service
@Slf4j
public class ConfigTemplateServiceImpl extends BeseServiceImpl<ConfigTemplateMapper, ConfigTemplateDO> implements ConfigTemplateService {

    @Autowired
    ConfigTemplateMapper configTemplateMapper;

    @Override
    public Result<ConfigTemplateDTO> getConfigTemplate(String configKey) {

        BestBpmAsset.isAssetEmpty(configKey);
        ConfigTemplateDO configTemplate = new ConfigTemplateDO();
        configTemplate.setTemplateKey(configKey);
        configTemplate.setValidState(VALID_STATE);
        QueryWrapper<ConfigTemplateDO> entityWrapper = new QueryWrapper<>(configTemplate);
        ConfigTemplateDO configTemplate1 = configTemplateMapper.selectOne(entityWrapper);
        BestBpmAsset.isEmpty(configTemplate1, "templatekey [" + configKey + "] no configTemplate in database");
        ConfigTemplateDTO configTemplateDTO = BeanUtils.switchToDTO(configTemplate1, ConfigTemplateDTO.class);
        return Result.responseSuccess(configTemplateDTO);
    }
}
