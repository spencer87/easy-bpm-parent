package com.pig.easy.bpm.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pig.easy.bpm.entity.ConfigTemplateDO;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author pig
 * @since 2020-05-21
 */
@Mapper
public interface ConfigTemplateMapper extends BaseMapper<ConfigTemplateDO> {

}
